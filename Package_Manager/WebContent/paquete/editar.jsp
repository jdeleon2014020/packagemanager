<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Package Manager</title>
</head>
<body>
	<form action="ServletEditarPaquete.do" method="get">
		<input type="hidden" name="txtIdPaquete" value="${paquete.getIdPaquete()}">
		Usuario: <input type="text" name="txtIdUsuario" value="${paquete.getIdUsuario()}">
		Ruta: <input type="text" name="txtIdRuta" value="${paquete.getIdRuta()}">
		Fecha: <input type="text" name="txtFecha" value="${paquete.getFecha()}">
		DireccionEmisor: <input type="text" name="txtDireccionEmisor" value="${paquete.getDireccionEmisor()}">
		TelefonoEmisor: <input type="text" name="txtTelefonoEmisor" value="${paquete.getTelefonoEmisor()}">
		DireccionReceptor: <input type="text" name="txtDireccionReceptor" value="${paquete.getDireccionReceptor()}">
		TelefonoReceptor: <input type="text" name="txtTelefonoReceptor" value="${paquete.getTelefonoReceptor()}">
		<input type="submit" value="Agregar">
	</form>
</body>
</html>