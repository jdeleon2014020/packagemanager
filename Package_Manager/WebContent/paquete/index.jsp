<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ taglib uri="http://java.sun.com/jsp/jstl/core"  prefix="c"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Package Manager</title>
</head>
<body>
	<h1>Listado de Paquetes</h1>
	<a href="../paquete/agregar.jsp">Nuevo Paquete</a>
	<table>
		<thead>
			<th>Usuario</th>
			<th>Ruta</th>
			<th>Fecha</th>
			<th>DireccionEmisor</th>
			<th>TelefonoEmisor</th>
			<th>DireccionReceptor</th>
			<th>TelefonoReceptor</th>
		</thead>
		<tbody>
			<c:forEach items="${listaPaquete}" var="paquete" >
				<tr>
					<td>${paquete.getIdUsuario()}</td>
					<td>${paquete.getIdRuta()}</td>
					<td>${paquete.getFecha()}</td>
					<td>${paquete.getDireccionEmisor()}</td>
					<td>${paquete.getTelefonoEmisor()}</td>
					<td>${paquete.getDireccionReceptor()}</td>
					<td>${paquete.getTelefonoReceptor()}</td>
					<td><a href="ServletEliminarPaquete.do?idPaquete=${paquete.getIdPaquete()}">Eliminar</a></td>
					<td><a href="ServletCargarPaquete.do?idPaquete=${paquete.getIdPaquete()}">Editar</a></td>
				</tr>
			</c:forEach>
		</tbody>
	</table>
</body>
</html>