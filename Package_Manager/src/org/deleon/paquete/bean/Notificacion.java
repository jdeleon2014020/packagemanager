package org.deleon.paquete.bean;

public class Notificacion {
	private Integer idNotificacion;
	private Integer idUsuario;
	private String descripcion;
	
	public Integer getIdNotificacion() {
		return idNotificacion;
	}
	public void setIdNotificacion(Integer idNotificacion) {
		this.idNotificacion = idNotificacion;
	}
	public Integer getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public Notificacion(Integer idNotificacion, Integer idUsuario, String descripcion) {
		super();
		this.idNotificacion = idNotificacion;
		this.idUsuario = idUsuario;
		this.descripcion = descripcion;
	}
	
	public Notificacion() {
		super();
		// TODO Auto-generated constructor stub
	}	
}
