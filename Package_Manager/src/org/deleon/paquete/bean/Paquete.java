package org.deleon.paquete.bean;

public class Paquete {
	private Integer idPaquete;
	private Integer idUsuario;
	private Integer idRuta;
	private String fecha;
	private String direccionEmisor;
	private Integer telefonoEmisor;
	private String direccionReceptor;
	private Integer telefonoReceptor;
	
	public Integer getIdPaquete() {
		return idPaquete;
	}
	public void setIdPaquete(Integer idPaquete) {
		this.idPaquete = idPaquete;
	}
	public Integer getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	public Integer getIdRuta() {
		return idRuta;
	}
	public void setIdRuta(Integer idRuta) {
		this.idRuta = idRuta;
	}
	public String getFecha() {
		return fecha;
	}
	public void setFecha(String fecha) {
		this.fecha = fecha;
	}
	public String getDireccionEmisor() {
		return direccionEmisor;
	}
	public void setDireccionEmisor(String direccionEmisor) {
		this.direccionEmisor = direccionEmisor;
	}
	public Integer getTelefonoEmisor() {
		return telefonoEmisor;
	}
	public void setTelefonoEmisor(Integer telefonoEmisor) {
		this.telefonoEmisor = telefonoEmisor;
	}
	public String getDireccionReceptor() {
		return direccionReceptor;
	}
	public void setDireccionReceptor(String direccionReceptor) {
		this.direccionReceptor = direccionReceptor;
	}
	public Integer getTelefonoReceptor() {
		return telefonoReceptor;
	}
	public void setTelefonoReceptor(Integer telefonoReceptor) {
		this.telefonoReceptor = telefonoReceptor;
	}
	public Paquete(Integer idPaquete, Integer idUsuario, Integer idRuta, String fecha, String direccionEmisor,
			Integer telefonoEmisor, String direccionReceptor, Integer telefonoReceptor) {
		super();
		this.idPaquete = idPaquete;
		this.idUsuario = idUsuario;
		this.idRuta = idRuta;
		this.fecha = fecha;
		this.direccionEmisor = direccionEmisor;
		this.telefonoEmisor = telefonoEmisor;
		this.direccionReceptor = direccionReceptor;
		this.telefonoReceptor = telefonoReceptor;
	}
	
	public Paquete() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	
	
	
}
