package org.deleon.paquete.bean;

public class Rol {
	private Integer idRol;
	private String nombre;
	private String apellido;
	private String descripcion;
	
	public Integer getIdRol() {
		return idRol;
	}
	public void setIdRol(Integer idRol) {
		this.idRol = idRol;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getApellido() {
		return apellido;
	}
	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
	public String getDescripcion() {
		return descripcion;
	}
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	public Rol(Integer idRol, String nombre, String apellido, String descripcion) {
		super();
		this.idRol = idRol;
		this.nombre = nombre;
		this.apellido = apellido;
		this.descripcion = descripcion;
	}
	
	public Rol() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
