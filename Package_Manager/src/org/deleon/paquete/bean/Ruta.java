package org.deleon.paquete.bean;

public class Ruta {
	private Integer idRuta;
	private String medio;
	private String tiempoEstimado;
	
	public Integer getIdRuta() {
		return idRuta;
	}
	public void setIdRuta(Integer idRuta) {
		this.idRuta = idRuta;
	}
	public String getMedio() {
		return medio;
	}
	public void setMedio(String medio) {
		this.medio = medio;
	}
	public String getTiempoEstimado() {
		return tiempoEstimado;
	}
	public void setTiempoEstimado(String tiempoEstimado) {
		this.tiempoEstimado = tiempoEstimado;
	}
	
	public Ruta(Integer idRuta, String medio, String tiempoEstimado) {
		super();
		this.idRuta = idRuta;
		this.medio = medio;
		this.tiempoEstimado = tiempoEstimado;
	}
	
	public Ruta() {
		super();
		// TODO Auto-generated constructor stub
	}
}
