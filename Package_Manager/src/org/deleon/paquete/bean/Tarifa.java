package org.deleon.paquete.bean;

public class Tarifa {
	private Integer idTarifa;
	private Integer idPaquete;
	private String producto;
	private Integer costo;
	
	public Integer getIdTarifa() {
		return idTarifa;
	}
	public void setIdTarifa(Integer idTarifa) {
		this.idTarifa = idTarifa;
	}
	public Integer getIdPaquete() {
		return idPaquete;
	}
	public void setIdPaquete(Integer idPaquete) {
		this.idPaquete = idPaquete;
	}
	public String getProducto() {
		return producto;
	}
	public void setProducto(String producto) {
		this.producto = producto;
	}
	public Integer getCosto() {
		return costo;
	}
	public void setCosto(Integer costo) {
		this.costo = costo;
	}
	public Tarifa(Integer idTarifa, Integer idPaquete, String producto, Integer costo) {
		super();
		this.idTarifa = idTarifa;
		this.idPaquete = idPaquete;
		this.producto = producto;
		this.costo = costo;
	}
	
	public Tarifa() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
