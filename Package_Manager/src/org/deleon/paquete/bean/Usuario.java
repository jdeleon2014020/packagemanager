package org.deleon.paquete.bean;

public class Usuario {
	private Integer idUsuario;
	private Integer idRol;
	private String contraseña;
	private String nick;
	private String direccion;
	private Integer telefono;
	private String correo;
	private Integer tarjetaCredito;
	
	public Integer getIdUsuario() {
		return idUsuario;
	}
	public void setIdUsuario(Integer idUsuario) {
		this.idUsuario = idUsuario;
	}
	public Integer getIdRol() {
		return idRol;
	}
	public void setIdRol(Integer idRol) {
		this.idRol = idRol;
	}
	public String getContraseña() {
		return contraseña;
	}
	public void setContraseña(String contraseña) {
		this.contraseña = contraseña;
	}
	public String getNick() {
		return nick;
	}
	public void setNick(String nick) {
		this.nick = nick;
	}
	public String getDireccion() {
		return direccion;
	}
	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}
	public Integer getTelefono() {
		return telefono;
	}
	public void setTelefono(Integer telefono) {
		this.telefono = telefono;
	}
	public String getCorreo() {
		return correo;
	}
	public void setCorreo(String correo) {
		this.correo = correo;
	}
	public Integer getTarjetaCredito() {
		return tarjetaCredito;
	}
	public void setTarjetaCredito(Integer tarjetaCredito) {
		this.tarjetaCredito = tarjetaCredito;
	}
	
	public Usuario(Integer idUsuario, Integer idRol, String contraseña, String nick, String direccion, Integer telefono,
			String correo, Integer tarjetaCredito) {
		super();
		this.idUsuario = idUsuario;
		this.idRol = idRol;
		this.contraseña = contraseña;
		this.nick = nick;
		this.direccion = direccion;
		this.telefono = telefono;
		this.correo = correo;
		this.tarjetaCredito = tarjetaCredito;
	}
	
	public Usuario() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
}
