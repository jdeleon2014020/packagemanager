package org.deleon.paquete.servlet.paquete;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.deleon.paquete.bean.Paquete;
import org.deleon.paquete.db.Conexion;

@WebServlet("/ServletAgregarPaquete.do")
public class ServletAgregarPaquete extends HttpServlet{
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher despachador=null;
		Paquete paquete=new Paquete(0,
				Integer.parseInt(req.getParameter("txtIdUsuario")),
				Integer.parseInt(req.getParameter("txtIdRuta")),
				req.getParameter("txtFecha"),
				req.getParameter("txtDireccionEmisor"),
				Integer.parseInt(req.getParameter("txtTelefonoEmisor")),
				req.getParameter("txtDireccionReceptor"),
				Integer.parseInt(req.getParameter("txtTelefonoReceptor")));
		Conexion.getInstancia().agregar(paquete);
		despachador=req.getRequestDispatcher("ServletListarPaquete.do");
		req.getSession().setAttribute("listadoDePaquetes", Conexion.getInstancia().listar("From Paquete"));
		despachador.forward(req, resp);
	}
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req,resp);
	
	}
}
