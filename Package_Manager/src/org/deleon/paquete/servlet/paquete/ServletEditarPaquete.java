package org.deleon.paquete.servlet.paquete;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.deleon.paquete.bean.Paquete;
import org.deleon.paquete.db.Conexion;

@WebServlet("/ServletEditarPaquete.do")
public class ServletEditarPaquete extends HttpServlet{
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher despachador=null;
		Paquete paquete=new Paquete(Integer.parseInt(req.getParameter("txtIdPaquete")),
				Integer.parseInt(req.getParameter("txtIdUsuario")),
				Integer.parseInt(req.getParameter("txtIdRuta")),
				req.getParameter("txtFecha"),
				req.getParameter("txtDireccionEmisor"),
				Integer.parseInt(req.getParameter("txtTelefonoEmisor")),
				req.getParameter("txtDireccionReceptor"),
				Integer.parseInt(req.getParameter("txtTelefonoReceptor")));
		Conexion.getInstancia().editar(paquete);
		despachador=req.getRequestDispatcher("ServletListarPaquete.do");
		despachador.forward(req, resp);
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req,resp);
	}
}
