package org.deleon.paquete.servlet.usuario;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.deleon.paquete.bean.Usuario;
import org.deleon.paquete.db.Conexion;

@WebServlet("/ServletAgregarUsuario.do")
public class ServletAgregarUsuario extends HttpServlet{
	
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher despachador=null;
		Usuario usuario=new Usuario(0,
				Integer.parseInt(req.getParameter("txtIdRol")),
				req.getParameter("txtContrase�a"),
				req.getParameter("txtNick"),
				req.getParameter("txtDireccion"),
				Integer.parseInt(req.getParameter("txtTelefono")),
				req.getParameter("txtCorreo"),
				Integer.parseInt(req.getParameter("txtTarjetaCredito")));
		Conexion.getInstancia().agregar(usuario);
		despachador=req.getRequestDispatcher("ServletListarUsuario.do");
		req.getSession().setAttribute("listadoDeUsuarios", Conexion.getInstancia().listar("From Usuario"));
		despachador.forward(req, resp);
	}
	
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req,resp);
	
	}
}
