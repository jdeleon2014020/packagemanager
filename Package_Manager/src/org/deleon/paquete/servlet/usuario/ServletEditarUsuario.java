package org.deleon.paquete.servlet.usuario;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.deleon.paquete.bean.Usuario;
import org.deleon.paquete.db.Conexion;

@WebServlet("/ServletEditarUsuario.do")
public class ServletEditarUsuario extends HttpServlet{
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		RequestDispatcher despachador=null;
		Usuario usuario=new Usuario(Integer.parseInt(req.getParameter("txtIdUsuario")),
				Integer.parseInt(req.getParameter("txtIdRol")),
				req.getParameter("txtContrase�a"),
				req.getParameter("txtNick"),
				req.getParameter("txtDireccion"),
				Integer.parseInt(req.getParameter("txtTelefono")),
				req.getParameter("txtCorreo"),
				Integer.parseInt(req.getParameter("txtTarjetaCredito")));
		Conexion.getInstancia().editar(usuario);
		despachador=req.getRequestDispatcher("ServletListarUsuario.do");
		despachador.forward(req, resp);
	}
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		doPost(req,resp);
	}
}
